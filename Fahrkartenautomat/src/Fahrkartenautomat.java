﻿import java.util.Scanner;
import java.util.ArrayList;

class Fahrkartenautomat
{
	
    public static void main(String[] args) {
        Scanner tastatur = new Scanner(System.in);
        // Achtung!!!!
        // Hier wird deutlich, warum double nicht für Geldbeträge geeignet ist.
        // =======================================
        double zuZahlenderBetrag;
        int anzahlTickets;
        double rückgabebetrag;

        // Den zu zahlenden Betrag ermittelt normalerweise der Automat
        // aufgrund der gewählten Fahrkarte(n).
        // -----------------------------------
        zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);
        // fahrkartenbestellungErfassen(tastatur);

        // Fahrkartenanzahl
        // ----------------
        anzahlTickets = fahrkartenAnzahl(tastatur);
        
        // Geldeinwurf
        // -----------
        rückgabebetrag = fahrkartenBezahlen(tastatur, zuZahlenderBetrag, anzahlTickets);

        // Fahrscheinausgabe
        // -----------------
        fahrkartenAusgeben();

        // Rückgeldberechnung und -Ausgabe
        // -------------------------------
        rueckgeldAusgeben(rückgabebetrag);

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wünschen Ihnen eine gute Fahrt.");
    }
    
    public static double fahrkartenbestellungErfassen(Scanner tastatur){
        int zielNummer;
        ArrayList<String> bezeichnungList = new ArrayList<String>();
        ArrayList<Double> moneyList = new ArrayList<Double>();
        // String[] bezeichnungList = new String [20];
        // double[] moneyList = new double [20];
        int antwortNeuerZielort;
        String bezeichnungNeuerZielort;
        double preisNeuerZielort;
        
        bezeichnungList.add("Einzelfahrschein Berlin AB");
        bezeichnungList.add("Einzelfahrschein Berlin BC");
        bezeichnungList.add("Einzelfahrschein Berlin ABC");
        bezeichnungList.add("Kurzstrecke");
        bezeichnungList.add("Tageskarte Berlin AB");
        bezeichnungList.add("Tageskarte Berlin BC");
        bezeichnungList.add("Tageskarte Berlin ABC");
        bezeichnungList.add("Kleingruppen-Tageskarte Berlin AB");
        bezeichnungList.add("Kleingruppen-Tageskarte Berlin BC");
        bezeichnungList.add("Kleingruppen-Tageskarte Berlin ABC");
        
        moneyList.add(2.90);
        moneyList.add(3.30);
        moneyList.add(3.60);
        moneyList.add(1.90);
        moneyList.add(8.60);
        moneyList.add(9.00);
        moneyList.add(9.60);
        moneyList.add(23.50);
        moneyList.add(24.30);
        moneyList.add(23.90);
        
        for (int i = 0; i < bezeichnungList.size(); i++) {
        	System.out.print(i + "." + bezeichnungList.get(i) + " - ");
        	System.out.printf("%.2f%s%n", moneyList.get(i), "€");
        }
        System.out.println("-----------------------------------------------");
        
        System.out.println("Wenn Sie einen neuen Zielort hinzufügen möchten, dann klicken Sie bitte auf die 1. Wenn Sie diesen Schritt überspringen möchten, dann klicken Sie auf die 2.");
		antwortNeuerZielort = tastatur.nextInt();
        if (antwortNeuerZielort == 1) {
        	System.out.println("Tragen Sie ihren neuen Zielort ein: ");
        	bezeichnungNeuerZielort = tastatur.next();
        	bezeichnungList.add(bezeichnungNeuerZielort);
        	System.out.println("Tragen Sie den Preis ein: ");
        	preisNeuerZielort = tastatur.nextDouble();
        	moneyList.add(preisNeuerZielort);
        }
        System.out.println("-----------------------------------------------");
        
        System.out.print("Wähle dein Zielort aus (0 - " + (bezeichnungList.size() - 1) + "): ");
        zielNummer = tastatur.nextInt();
        System.out.println("-----------------------------------------------");
        
        System.out.printf("%s%s%.2f%s%n", bezeichnungList.get(zielNummer), " - ", moneyList.get(zielNummer), "€");
        System.out.println("-----------------------------------------------");
        
        return moneyList.get(zielNummer);
        
        //Jetzt kann man ganz einfach neue Zielorte hinzufügen/löschen. Das Programm ist damit dynamischer geworden.
        //Der Nachteil an den Arrays ist, dass sie viel mehr Quellcode verbrauchen & unübersichtlicher sind. Der Vorteil ist im oberen Kommentar genannt.
    }
    
    public static int fahrkartenAnzahl(Scanner tastatur) {
    	int anzahl;
    	
    	System.out.print("Anzahl der Tickets: ");
        anzahl = tastatur.nextInt();
        return anzahl;
    }
    
    private static double fahrkartenBezahlen(Scanner tastatur, double zuZahlenderBetrag, int anzahl) {
    	double eingezahlterGesamtbetrag = 0.0;
        double eingeworfeneMünze;
        zuZahlenderBetrag = zuZahlenderBetrag * anzahl;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
        	System.out.printf("%s%.2f%s%n", "Noch zu zahlen:", (zuZahlenderBetrag - eingezahlterGesamtbetrag), "€" );
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }
    
    private static void rueckgeldAusgeben(double rückgabebetrag) {
        if(rückgabebetrag > 0.0)
        {
        	System.out.printf("%s%.2f%s%n", "Der Rückgabebetrag in Höhe von:", rückgabebetrag, "€");
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
                System.out.println("2 EURO");
                rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
                System.out.println("1 EURO");
                rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                System.out.println("50 CENT");
                rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                System.out.println("20 CENT");
                rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                System.out.println("10 CENT");
                rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                System.out.println("5 CENT");
                rückgabebetrag -= 0.05;
            }
            while(rückgabebetrag >= 0.02) // 2 CENT-Münzen
            {
         	  System.out.println("2 CENT");
  	          rückgabebetrag -= 0.02;
            }
            while(rückgabebetrag >= 0.01) // 1 CENT-Münzen
            {
         	  System.out.println("1 CENT");
  	          rückgabebetrag -= 0.01;
            }
            while(rückgabebetrag >= 0.001) // 1 CENT-Münzen
            {
         	  System.out.println("1 CENT");
  	          rückgabebetrag -= 0.01;
            }
        }
    }

    private static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }
}
// https://bitbucket.org/Sieder_/ls01_fi-c-02_sieder_rep/src/master/Fahrkartenautomat/src/Fahrkartenautomat.java