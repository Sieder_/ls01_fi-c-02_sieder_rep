
public class Test {

	public static void main(String[] args)
	{
  /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
        Vereinbaren Sie eine geeignete Variable */
		int zaehler;

  /* 2. Weisen Sie dem Zaehler den Wert 25 zu
        und geben Sie ihn auf dem Bildschirm aus.*/
		zaehler = 25;
		// System.out.println(zaehler);

  /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
        eines Programms ausgewaehlt werden.
        Vereinbaren Sie eine geeignete Variable */
		char auswahl;

  /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
        und geben Sie ihn auf dem Bildschirm aus.*/
		auswahl = 'C';
		// System.out.println(auswahl);

  /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
        notwendig.
        Vereinbaren Sie eine geeignete Variable */
		double astronomischGrosseZahl;

  /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
        und geben Sie sie auf dem Bildschirm aus.*/
		astronomischGrosseZahl = 2.9979E8;
		// System.out.println("Lichtgeschwindigkeit: " + astronomischGrosseZahl + "m/s");

  /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
        soll die Anzahl der Mitglieder erfasst werden.
        Vereinbaren Sie eine geeignete Variable und initialisieren sie
        diese sinnvoll.*/
		short mitgliederZahl = 7;

  /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
		// System.out.println(mitgliederZahl);

  /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
        Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
        dem Bildschirm aus.*/
		final double elektrischeElementarladung = 1.602E-19d;
		// System.out.println("elektrische Elementarladungen: "+elektrischeElementarladung+" C" );

  /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
        Vereinbaren Sie eine geeignete Variable. */
		boolean istBezahlt;

  /*11. Die Zahlung ist erfolgt.
        Weisen Sie der Variable den entsprechenden Wert zu
        und geben Sie die Variable auf dem Bildschirm aus.*/
		istBezahlt = true;
		// System.out.println("Rechnung ist Bezahlt: " + istBezahlt);
		
		
		String example = "";
		// System.out.println(example);
		
		
		
  /*1. Funktionen/Methoden */
		/*double x = 3.0;
		double y = 4.0;
		
		double mittelwert = (x + y) / 2.0;
		
		System.out.println("Mittelwert: " + mittelwert);

		
		double mittel = mittelwert (x, y);
		System.out.println("Mittelwert: " + mittel);
		
		static double mittelwert (double a, double b)(
		)				return (a + b) / 2.0;		
		 */
		
		System.out.println("Vor dem Methodenaufruf");
		doSomething(20, 20);
		System.out.println("Nach dem Methodenaufruf");
		
		 
		System.out.println(doSomething(12,10));
	}
	//ohne Parameter
	/* public static void doSomething() { //void --> nichts wird zur�ckgegeben
		int x = 10;
		int y = 20;
		int result = x + y;
		System.out.println(result); */
		
	//mit Parameter
	/* public static void doSomething(int number1, int number2) { //void --> nichts wird zur�ckgegeben
		int x = number1;
		int y = number2;
		int result = x + y;
		System.out.println(result);
	} */
	
		//mit R�ckgabewert
	   public static int doSomething(int number1, int number2) { //Datentyp --> wird was zur�ckgegeben
		return number1 + number2;
} 
}
