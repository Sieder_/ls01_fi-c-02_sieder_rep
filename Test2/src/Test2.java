
public class Test2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Hello World");
		// Aufgabe 1
				System.out.printf("   **   ");
				System.out.printf("\n*      *");
				System.out.printf("\n*      *");
				System.out.printf("\n   **   %n%n");
				
				// Aufgabe 2
				System.out.printf("%-5s=", "0!");
				System.out.printf("%-19s=", "");
				System.out.printf("%4s%n", "1");
				
				System.out.printf("%-5s=", "1!");
				System.out.printf("%-19s=", " 1");
				System.out.printf("%4s%n", "1");
				
				System.out.printf("%-5s=", "2!");
				System.out.printf("%-19s=", " 1 * 2");
				System.out.printf("%4s%n", "2");
				
				System.out.printf("%-5s=", "3!");
				System.out.printf("%-19s=", " 1 * 2 * 3");
				System.out.printf("%4s%n", "6");
				
				System.out.printf("%-5s=", "4!");
				System.out.printf("%-19s=", " 1 * 2 * 3 * 4");
				System.out.printf("%4s%n", "24");
				
				System.out.printf("%-5s=", "5!");
				System.out.printf("%-19s=", " 1 * 2 * 3 * 4 * 5");
				System.out.printf("%4s%n%n", "120");
				
				// Aufgabe 3
				System.out.printf("%-12s|%10s%n", "Fahrenheit", "Celsius");
				/* System.out.printf("%22s%n", "-----------------------");
				System.out.printf("%-12d|%10.2f%n", -20, -28.8889);
				System.out.printf("%-12d|%10.2f%n", -10, -23.3333);
				System.out.printf("%-+12d|%10.2f%n", 0, -17.7778);
				System.out.printf("%-+12d|%10.2f%n", 20, -6.6667);
				System.out.printf("%-+12d|%10.2f%n", 30, -1.1111);
				*/
				
				System.out.printf("%.23s%n", "------------------------------------------");
				System.out.printf("%-12d|%10.2f%n", -20, -28.8889);
				System.out.printf("%-12d|%10.2f%n", -10, -23.3333);
	}

}
