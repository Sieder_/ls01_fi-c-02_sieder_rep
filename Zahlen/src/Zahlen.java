import java.util.Scanner;

public class Zahlen {

	public static void main(String[] args) {
		//Aufgabe 1
		zahlen();
		//Aufgabe 2
		ungeradeZahlen();
		//Aufgabe 3
		palindrom();
		//Aufgabe 4
		lotto();
	}

	//Aufgabe 1
	public static void zahlen() {
		int[] list = new int [10];
		int listLength = list.length;
		
		for (int i = 0; i < listLength; i++) {
			list[i] = i;
			System.out.print(list[i] + letztesKomma(i, listLength));
		}
	}

	public static String letztesKomma(int i, int listLength) {
		if (i != (listLength - 1)) {
			return ",";
		} else {
			return "";
		}
	}

	//Aufgabe 2
	public static void ungeradeZahlen() {
		int[] list = new int [10];
		
		for (int i = 1; i <= 19; i++) {
			if (i % 2 == 1) {
				System.out.println(i);
			}
		}
	}
	//Aufgabe 3
	public static void palindrom() {
		Scanner tastatur = new Scanner(System.in);
		char[] list = new char[5];
		
		for (int i = 0; i < list.length; i++) {
			System.out.print("Gebe ein beliebiges Zeichen ein: ");
			char zeichen = tastatur.next().charAt(0);
			list[i] = zeichen;
		}
		for (int i = list.length - 1; i >= 0; i--) {
			System.out.print(list[i]);
		}
	}
	//Aufgabe 4
	public static void lotto() {
		int[] lottoList = new int[6];
		boolean zwölfGefunden = false;
		
		int number1 = 3;
		int number2 = 7;
		int number3 = 12;
		int number4 = 18;
		int number5 = 37;
		int number6 = 42;
		
		for (int i = 0; i < lottoList.length; i++) {
			switch(i) {
			case 0:
				System.out.print(number1 + " ");
				lottoList[i] = number1;
				break;
			case 1:
				System.out.print(number2 + " ");
				lottoList[i] = number2;
				break;
			case 2:
				System.out.print(number3 + " ");
				lottoList[i] = number3;
				break;
			case 3:
				System.out.print(number4 + " ");
				lottoList[i] = number4;
				break;
			case 4:
				System.out.print(number5 + " ");
				lottoList[i] = number5;
				break;
			case 5:
				System.out.print(number6 + " ");
				lottoList[i] = number6;
				break;
			}
		}
		for (int i : lottoList) {
			if (i == 12) {
				zwölfGefunden = true;
			}
		}
		if (zwölfGefunden) {
			System.out.println("Die Zahl 12 wurde gefunden.");
		} else {
			System.out.println("Die Zahl 12 wurde nicht gefunden.");
		}
	}

}
