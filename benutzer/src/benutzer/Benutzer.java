package benutzer;

public class Benutzer {
	private String name;
	private String eMail;
	private String passwort;
	private String adresse;
	private String berechtigungsstatus;
	private String anmeldestatus;
	private String letzterLogin;
	
	public Benutzer() {
		setName(name);
		setEMail(eMail);
		setPasswort(passwort);
		setAdresse(adresse);
		setBerechtigungsstatus(berechtigungsstatus);
		setAnmeldestatus(anmeldestatus);
		setLetzterLogin(letzterLogin);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getEMail() {
		return eMail;
	}
	public void setEMail(String eMail) {
		this.eMail = eMail;
	}
	
	public String getPasswort() {
		return passwort;
	}
	public void setPasswort(String passwort) {
		this.passwort = passwort;
	}
	
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	
	public String getBerechtigungsstatus() {
		return berechtigungsstatus;
	}
	public void setBerechtigungsstatus(String berechtigungsstatus) {
		this.berechtigungsstatus = berechtigungsstatus;
	}
	
	public String getAnmeldestatus() {
		return anmeldestatus;
	}
	public void setAnmeldestatus(String anmeldestatus) {
		this.anmeldestatus = anmeldestatus;
	}
	
	public String getLetzterLogin() {
		return letzterLogin;
	}
	public void setLetzterLogin(String letzterLogin) {
		this.letzterLogin = letzterLogin;
	}
}
