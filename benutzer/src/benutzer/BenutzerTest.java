package benutzer;

public class BenutzerTest {

	public static void main(String[] args) {
		Benutzer p1 = new Benutzer();
		
		p1.setName("M�ller");
		System.out.println(p1.getName());
		
		p1.setEMail("m�ller.maske@gmail.com");
		System.out.println(p1.getEMail());
		
		p1.setPasswort("Passwort");
		System.out.println(p1.getPasswort());
		
		p1.setAdresse("Trebbin 14959 Trebbinerstr. 89");
		System.out.println(p1.getAdresse());
		
		p1.setBerechtigungsstatus("Hoch");
		System.out.println(p1.getBerechtigungsstatus());
		
		p1.setAnmeldestatus("Fehlgeschlagen");
		System.out.println(p1.getAnmeldestatus());
		
		p1.setLetzterLogin("12-01-2021");
		System.out.println(p1.getLetzterLogin());
	}

}
