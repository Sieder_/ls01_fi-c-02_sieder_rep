import java.util.Scanner;

public class Kontrollstrukturen {
	//Aufgabe 1
	static int monatsZahl = 13;
	
	public static void main(String[] args) {
		// Aufgabe 1
		// System.out.println("Es ist " + ifMonat(monatsZahl) + ".");
		// System.out.println("Es ist " + caseMonat(monatsZahl) + ".");
		// Aufgabe 2
		// steuersatz();
		// Aufgabe 3
		kosten();
	}
	//Aufgabe 1
	public static String ifMonat(int monatsZahl) {
		if (monatsZahl == 1) {
			return "Januar";
		}
		if (monatsZahl == 2) {
			return "Februar";
		}
		if (monatsZahl == 3) {
			return "Maerz";
		}
		if (monatsZahl == 4) {
			return "April";
		}
		if (monatsZahl == 5) {
			return "Mai";
		}
		if (monatsZahl == 6) {
			return "Juni";
		}
		if (monatsZahl == 7) {
			return "Juli";
		}
		if (monatsZahl == 8) {
			return "August";
		}
		if (monatsZahl == 9) {
			return "September";
		}
		if (monatsZahl == 10) {
			return "Oktober";
		}
		if (monatsZahl == 11) {
			return "November";
		}
		if (monatsZahl == 12) {
			return "Dezember";
		}
		return "falsch";
	}
	
	public static String caseMonat(int monatsZahl) {
		switch(monatsZahl) {
		case 1:
			return "Januar";
		case 2:
			return "Februar";
		case 3:
			return "Maerz";
		case 4:
			return "April";
		case 5:
			return "Mai";
		case 6:
			return "Juni";
		case 7:
			return "Juli";
		case 8:
			return "August";
		case 9:
			return "September";
		case 10:
			return "Oktober";
		case 11:
			return "November";
		case 12:
			return "Dezember";
		default:
			return "falsch";
		}
	}
	//Aufgabe 2
	public static void steuersatz() {
		double nettowert;
		char vollerSteuersatz;
		Scanner tastatur = new Scanner(System.in);
		
		System.out.print("Dein Nettowert ist: ");
		nettowert = tastatur.nextDouble();
		System.out.print("Voller Steuersatz (J/N): ");
		vollerSteuersatz = tastatur.next().charAt(0);
		
		if (vollerSteuersatz == 'J') {
			double vollerSteuersatzResult = ((nettowert * 7) / 100) + nettowert;
			
			System.out.printf("%s%.2f%s", "Es kostet ", vollerSteuersatzResult, "�.");
		} else {
			if (vollerSteuersatz == 'N') {
				double maessigerSteuersatzResult = ((nettowert * 19) / 100) + nettowert;
				System.out.printf("%s%.2f%s", "Es kostet ", maessigerSteuersatzResult, "�.");
			}
		}
	}
	//Aufgabe 4
	public static void kosten() {
		double anzahlMaeuse;
		double preisMaus = 50;
		double vollerPreisMaus;
		
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Anzahl der Maeuse: ");
		anzahlMaeuse = tastatur.nextInt();
		if (anzahlMaeuse > 10) {
			vollerPreisMaus = (((preisMaus * 19) / 100) + preisMaus) * anzahlMaeuse;
			System.out.printf("%s%.2f", "Rechnung: ", vollerPreisMaus);
		} else {
			vollerPreisMaus = (((preisMaus * 19) / 100) + preisMaus) * anzahlMaeuse + 10;
			System.out.printf("%s%.2f", "Rechnung: ", vollerPreisMaus);
		}		
	}
}
